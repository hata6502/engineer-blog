import {
  Table,
  Column,
  Model,
  DataType,
  AllowNull,
  HasMany,
  CreatedAt,
  UpdatedAt,
  PrimaryKey,
  AutoIncrement,
} from 'sequelize-typescript';

import Post from '../post';

@Table
export default class Author extends Model<Author> {
  @PrimaryKey
  @AutoIncrement
  @Column
  public id!: number;

  @AllowNull(false)
  @Column(DataType.STRING)
  public name!: string;

  @HasMany(() => Post)
  public posts!: Post[];

  @CreatedAt
  public createdAt!: Date;

  @UpdatedAt
  public updatedAt!: Date;
}
