import Author from './model';
import authorQueries from './queries';
import authorTypeDefs from './schema';
import authorMutations from './mutations';

export default Author;
export { authorTypeDefs, authorQueries, authorMutations };
