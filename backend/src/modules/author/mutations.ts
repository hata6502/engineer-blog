import Author from './model';
import { MutationResolvers } from '../../generated/types';

const authorMutations: MutationResolvers = {
  createAuthor: async (_parent, { name }, _context) => {
    const now = new Date();
    const author = await Author.create({
      name,
      createdAt: now,
      updatedAt: now,
    });

    return author;
  },
};

export default authorMutations;
