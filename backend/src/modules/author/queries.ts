import Author from './model';
import Post from '../post';
import { QueryResolvers } from '../../generated/types';

const authorQueries: QueryResolvers = {
  author: async (_parent, { id }, _context) => {
    const author = await Author.findByPk(id, {
      include: [{ model: Post }],
    });

    if (!author) {
      throw new Error('author not found');
    }

    return author;
  },
};

export default authorQueries;
