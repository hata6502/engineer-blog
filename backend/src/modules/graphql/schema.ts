import { gql } from 'apollo-server';
import { GraphQLDateTime } from 'graphql-iso-date';

import { Resolvers } from '../../generated/types';
import { authorTypeDefs, authorQueries, authorMutations } from '../author';
import { postTypeDefs, postQueries, postMutations } from '../post';

const initTypeDefs = gql`
  scalar DateTime

  type Query {
    _empty: String
  }
  type Mutation {
    _empty: String
  }
`;

const typeDefs = [initTypeDefs, postTypeDefs, authorTypeDefs];

const resolvers: Resolvers = {
  DateTime: GraphQLDateTime,

  Query: {
    ...postQueries,
    ...authorQueries,
  },
  Mutation: {
    ...postMutations,
    ...authorMutations,
  },
};

export { typeDefs, resolvers };
