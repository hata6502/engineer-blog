import {
  Table,
  Column,
  Model,
  DataType,
  AllowNull,
  BelongsTo,
  ForeignKey,
  PrimaryKey,
  CreatedAt,
  UpdatedAt,
  AutoIncrement,
} from 'sequelize-typescript';

import { PostStatus } from '../../generated/types';
import Author from '../author';

@Table
export default class Post extends Model<Post> {
  @PrimaryKey
  @AutoIncrement
  @Column
  public id!: number;

  @AllowNull(false)
  @Column
  public title!: string;

  @Column
  public thumbnailUrl!: string;

  @AllowNull(false)
  @Column(DataType.TEXT)
  public content!: string;

  @AllowNull(false)
  @Column(DataType.ENUM(...Object.values(PostStatus)))
  public status!: PostStatus;

  @AllowNull(false)
  @ForeignKey(() => Author)
  @Column
  public authorId!: number;

  @BelongsTo(() => Author)
  public author!: Author;

  @CreatedAt
  public createdAt!: Date;

  @UpdatedAt
  public updatedAt!: Date;
}
