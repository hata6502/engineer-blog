import Post from './model';
import Author from '../author';
import { MutationResolvers } from '../../generated/types';

const postMutations: MutationResolvers = {
  createPost: async (_parent, { title, thumbnailUrl, content, status, authorId }, _context) => {
    const now = new Date();
    const author = await Author.findByPk(authorId);

    if (!author) {
      throw new Error('Author not found');
    }

    const post = await Post.create<Post>({
      title,
      thumbnailUrl,
      content,
      status,
      authorId,
      createdAt: now,
      updatedAt: now,
    });

    post.author = author;

    return post;
  },
};

export default postMutations;
