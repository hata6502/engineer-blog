import Post from './model';
import postMutations from './mutations';
import postQueries from './queries';
import postTypeDefs from './schema';

export default Post;
export { postTypeDefs, postQueries, postMutations };
