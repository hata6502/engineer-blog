import { gql } from 'apollo-server';

const postTypeDef = gql`
  enum PostStatus {
    published
    private
  }

  extend type Query {
    post(id: Int!): Post!
  }

  extend type Mutation {
    createPost(title: String!, thumbnailUrl: String, content: String!, status: PostStatus!, authorId: Int!): Post!
  }

  type Post {
    id: Int!
    title: String!
    thumbnailUrl: String
    content: String!
    status: PostStatus!
    authorId: Int!
    createdAt: DateTime!
    updatedAt: DateTime!
    author: Author!
  }
`;

export default postTypeDef;
