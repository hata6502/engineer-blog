/* eslint-disable prefer-destructuring */
type NODE_ENV = 'development' | 'testing' | 'staging' | 'production';

// 環境
export const NODE_ENV = process.env.NODE_ENV as NODE_ENV;

// DB
export const MYSQL_DATABASE = process.env.MYSQL_DATABASE;
export const MYSQL_HOST = process.env.MYSQL_HOST;
export const MYSQL_USER = process.env.MYSQL_USER;
export const MYSQL_PASSWORD = process.env.MYSQL_PASSWORD;
