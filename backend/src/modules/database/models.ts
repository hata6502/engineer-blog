import { Sequelize } from 'sequelize-typescript';
import Post from '../post';
import Author from '../author';
import { NODE_ENV } from '../settings';

export default async (sequelize: Sequelize) => {
  sequelize.addModels([Post, Author]);

  // TODO: 環境変数をsettingsモジュールに分けて
  if (NODE_ENV === 'development' || NODE_ENV === 'testing') {
    await sequelize.sync({ alter: true });
  }

  return sequelize;
};
