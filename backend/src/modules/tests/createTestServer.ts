import { ApolloServer } from 'apollo-server';
import { ApolloServerExpressConfig } from 'apollo-server-express';
import { typeDefs, resolvers } from '../graphql';

const createTestServer = (context: ApolloServerExpressConfig['context'] = {}) => {
  const server = new ApolloServer({
    typeDefs,
    resolvers,
    context,
  });

  return server;
};

export default createTestServer;
