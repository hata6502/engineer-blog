# バックエンド

## 使用技術
- docker
- docker-compose
- apollo-server
- eslint
- graphql-codegen
- jest
- mysql
- prettier
- sequelize
- sqlite3
- typescript

## 開発
.env.example をもとに .env を編集して環境設定をします。
`docker-compose up -d` によって開発環境を起動します。
http://localhost:50080/ から GraphQL Playground にアクセスできます。

### コマンド
- `yarn generate`: GraphQL の型定義を生成します。
- `yarn fix`: 自動整形および lint を実行します。
- `yarn test`: テストを実行します。
- `yarn snapshot`: テスト用のスナップショットを作成します。
