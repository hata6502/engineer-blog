# エンジニアブログ

## 使用技術
- BitBucket Pipelines

## ディレクトリ構成
```
.
├── README.md
├── bitbucket-pipelines.yml
├── backend
└── frontend
```

バックエンド、フロントエンドについては [backend/README.md](backend/README.md)、[frontend/README.md](frontend/README.md) を参照してください。
