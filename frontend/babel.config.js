module.exports = api => ({
  presets: [
    '@babel/env',
    ['@babel/typescript', { isTSX: true, allExtensions: true }],
    ['@babel/react', { development: api.env('development') }],
  ],
  plugins: [
    '@babel/plugin-syntax-dynamic-import',
    '@babel/proposal-class-properties',
    '@babel/proposal-object-rest-spread',
  ],
});
