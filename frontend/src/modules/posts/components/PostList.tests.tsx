import React from 'react';
import { render, waitForElement, act } from '@testing-library/react';
import { MockedProvider, MockedResponse } from '@apollo/react-testing';
import PostList, { GET_POST } from './PostList';

const dataMock: MockedResponse = {
  request: {
    query: GET_POST,
    variables: {
      id: 1,
    },
  },
  result: {
    data: {
      post: {
        id: 1,
        link: 'test-link',
      },
    },
  },
};

const errorMock: MockedResponse = {
  request: {
    query: GET_POST,
    variables: {
      id: 1,
    },
  },
  error: new Error('post not found'),
};

describe('PostList', () => {
  it('should render loading state', async () => {
    // @ts-ignore
    await act(async () => {
      const { getByText } = render(
        <MockedProvider mocks={[]}>
          <PostList />
        </MockedProvider>,
      );

      const loading = getByText('読み込み中...');

      expect(loading).toMatchSnapshot();
    });
  });

  it('should render error state', async () => {
    // @ts-ignore
    await act(async () => {
      const { getByText } = render(
        <MockedProvider mocks={[errorMock]}>
          <PostList />
        </MockedProvider>,
      );

      const error = await waitForElement(() => getByText('エラーが発生しました。'));

      expect(error).toMatchSnapshot();
    });
  });

  it('should render data state', async () => {
    // @ts-ignore
    await act(async () => {
      const { getByText } = render(
        <MockedProvider mocks={[dataMock]} addTypename={false}>
          <PostList />
        </MockedProvider>,
      );

      const data = await waitForElement(() => getByText('test-link'));

      expect(data).toMatchSnapshot();
    });
  });
});
