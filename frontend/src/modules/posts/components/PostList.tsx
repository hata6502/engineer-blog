import React from 'react';
import gql from 'graphql-tag';
import { useQuery } from 'react-apollo';

interface Post {
  id: number;
  link: string;
}

export const GET_POST = gql`
  query GetPost($id: Int!) {
    post(id: $id) {
      id
      link
    }
  }
`;

interface GetPost {
  post: Post;
}

interface GetPostVars {
  id: number;
}

const PostList: React.FunctionComponent = () => {
  const { data, loading, error } = useQuery<GetPost, GetPostVars>(GET_POST, { variables: { id: 1 } });

  if (loading) {
    return <p>読み込み中...</p>;
  }

  if (error || !data) {
    return <p>エラーが発生しました。</p>;
  }

  return <p>{data.post.link}</p>;
};

export default PostList;
