import React from 'react';
import styled from 'styled-components';

const Div = styled.div`
  width: 50%;
  padding: 0.5rem 1rem;
  margin: 0 auto 2rem;
  border: 2px solid #ddd;

  &::before {
    font-weight: bold;
    content: '目次';
  }
`;

const Ol = styled.ol`
  counter-reset: table-of-contents-counter;
`;

const Li = styled.li`
  position: relative;
  margin-left: 2rem;
  list-style: none;

  &::before {
    position: absolute;
    top: 0;
    left: -2rem;
    width: 2rem;
    padding-right: 0.5rem;
    text-align: right;
    content: counters(table-of-contents-counter, '.');
    counter-increment: table-of-contents-counter;
  }
`;

const PostTableOfContents: React.FunctionComponent = () => {
  return (
    <>
      <Div>
        <Ol>
          <Li>
            setroubled のインストール（アンカーリンクにしたい）
            <Ol>
              <Li>
                CentOS (yum) の場合
                <Ol />
              </Li>
              <Li>
                Ubuntu (apt) の場合
                <Ol />
              </Li>
            </Ol>
          </Li>
          <Li>
            権限エラーに対処してみる
            <Ol />
          </Li>
        </Ol>
      </Div>
    </>
  );
};

export default PostTableOfContents;
