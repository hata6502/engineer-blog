import React from 'react';
import styled from 'styled-components';

const Span = styled.span`
  font-size: large;
  font-weight: bold;
  color: ${props => props.theme.colors.danger};
`;

const PostBadge: React.FunctionComponent = () => {
  return (
    <>
      <Span>公開</Span>
    </>
  );
};

export default PostBadge;
