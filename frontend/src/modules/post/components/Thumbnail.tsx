import React from 'react';
import styled from 'styled-components';

const HorizontalContainer = styled.div`
  max-width: 780px;
  margin-right: auto;
  margin-bottom: 2rem;
  margin-left: auto;
`;

const VerticalContainer = styled.div`
  position: relative;
  padding-top: 56.25%;
  overflow: hidden;
`;

const Img = styled.img`
  position: absolute;
  top: 0;
  bottom: 0;
  width: 100%;
  height: auto;
  margin: auto;
`;

const PostThumbnail: React.FunctionComponent = () => {
  return (
    <>
      <HorizontalContainer>
        <VerticalContainer>
          <Img src="/images/public/domainq-0033354fvanhv.jpg" alt="記事サムネイル" />
        </VerticalContainer>
      </HorizontalContainer>
    </>
  );
};

export default PostThumbnail;
