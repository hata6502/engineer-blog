import React from 'react';
import Helmet from 'react-helmet';
import { ThemeProvider, DefaultTheme, createGlobalStyle } from 'styled-components';
import { Normalize } from 'styled-normalize';
import Post from '../../modules/post/components/Post';

const theme: DefaultTheme = {
  colors: {
    primary: 'blue',
    danger: 'tomato',
  },
  contentWidth: '984px',
};

const GlobalStyle = createGlobalStyle`
  body {
    font-family: 'ヒラギノ角ゴシック Pro', 'Hiragino Kaku Gothic Pro', '游ゴシック Medium', 'Yu Gothic Medium', sans-serif;
    color: #222;
  }
`;

const App = () => (
  <>
    <Helmet defaultTitle="エンジニアブログ" titleTemplate="%s | エンジニアブログ">
      <meta charSet="utf-8" />
      <title>記事</title>
    </Helmet>

    <Normalize />
    <GlobalStyle />

    <ThemeProvider theme={theme}>
      <Post />
    </ThemeProvider>
  </>
);

export default App;
