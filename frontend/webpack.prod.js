/* eslint-disable @typescript-eslint/no-var-requires */
const merge = require('webpack-merge');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const common = require('./webpack.common.js');

module.exports = merge(common, {
  mode: 'production',
  output: {
    filename: 'runtime.[contenthash].js',
    path: `${__dirname}/dist/static/js`,
    publicPath: '/static/js/',
    chunkFilename: '[name].[contenthash].js',
  },
  plugins: [
    new HtmlWebpackPlugin({
      filename: `${__dirname}/dist/index.html`,
      template: './src/index.html',
    }),
  ],
});
