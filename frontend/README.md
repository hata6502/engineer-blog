# フロントエンド開発用ディレクトリ

## 構造

- `src/`
  - ソースコード置き場
- `dist/`
  - コンパイルされたファイル置き場
- `node_modules/`
  - 外部モジュール置き場
- `.env`
  - docker の環境設定ファイル
- `.env.example`
  - .env の設定例
- `docker-dompose.dev.yml`
  - 開発環境用 docker-compose 設定ファイル
- `docker-dompose.yml`
  - ステージング／本番環境用 docker-compose 設定ファイル
- `package-lock.json`
  - インストール済モジュールのバージョン情報とか依存関係とか
- `package.json`
  - リポジトリ設定、npm の設定、モジュールの依存関係 etc.
- `README.md`
  - これ
- `tsconfig.json`
  - typescript のコンパイル設定
- `webpack.config.js`
  - webpack とか webpack-dev-server とかの設定

---

## .tsx ファイルのコンパイル

### 開発環境

- `docker-compose -p engineer-blog up -d`
- ホットリロードあり
- コンソールで上記コマンド実行後、ブラウザで下記の URL を開いて動作確認。  
  `http://localhost:{.envで指定したポート}`
